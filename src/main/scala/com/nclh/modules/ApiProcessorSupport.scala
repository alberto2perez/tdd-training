package com.nclh.modules

import akka.actor.ActorRef
import akka.util.Timeout
import com.ncl.services.user.UserRegistry
import com.nclh.services.cache.Cache

import scala.concurrent.ExecutionContext

/**
  * Contains all common variables that are shared among Routes
  */
trait ApiProcessorSupport {
    implicit val ec: ExecutionContext
    implicit val timeout: Timeout
    val apiProcessor: ActorRef
    val cacheOpt: Option[Cache]
    val usersService: UserRegistry
}
