package com.nclh.modules

import akka.actor.{ActorRef, ActorSystem}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.ncl.services.user.UserRegistry
import com.nclh.config.SystemConfig.HttpServerConfig
import com.nclh.routes.{DocRoutes, ToolsRoutes, WeatherRoutes}
import com.nclh.services.cache.Cache
import com.nclh.utils.{ExceptionResponseHandler, RejectionResponseHandler}

import scala.concurrent.ExecutionContext

class ApiModule(val apiProcessor: ActorRef, val usersService: UserRegistry, val cacheOpt: Option[Cache])(
    implicit val ec: ExecutionContext,
    implicit val timeout: akka.util.Timeout,
    implicit val system: ActorSystem)

    extends RejectionResponseHandler
        with ExceptionResponseHandler
        with ToolsRoutes
        with DocRoutes
        with WeatherRoutes
{

    val routes: Route = handleExceptions(exceptionHandler) {
        handleRejections(rejectionHandler) {
            encodeResponse {
                pathPrefix(HttpServerConfig.apiPrefix) {
                    apiRoutes
                } ~ toolsRoute
            }
        }
    }

    protected def apiRoutes: Route = {
        pathPrefix(HttpServerConfig.apiVersion) {
            docRoutes ~ weatherRoutes
        }
    }
}
