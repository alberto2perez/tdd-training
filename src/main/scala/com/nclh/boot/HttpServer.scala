package com.nclh.boot

import akka.actor.{ActorRef, ActorSystem}
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import akka.util.Timeout
import com.nclh.config.SystemConfig
import com.nclh.config.SystemConfig.ServerConfig
import com.nclh.modules.ApiModule
import com.nclh.services.cache.{CacheImpl, StorageImpl}
import com.nclh.services.user.{UserRegistryImpl, UserStorageMockImpl}
import com.nclh.services.{ApiRequestProcessor, RequestServerImpl}
import com.typesafe.scalalogging.LazyLogging
import scalacache.caffeine.CaffeineCache
import scalacache.{Cache => ScalaCache}

import scala.concurrent.Await
import scala.concurrent.duration._
import scala.util.control.NonFatal

object HttpServer extends App with LazyLogging {

    implicit val system: ActorSystem = ActorSystem(ServerConfig.systemName)
    implicit val materializer: ActorMaterializer = ActorMaterializer()
    implicit val timeout: Timeout = Timeout(ServerConfig.defaultTimeout.seconds)
    implicit val executionContext = system.dispatcher

    val backendRequestServer = new RequestServerImpl

    implicit val cache = if (SystemConfig.BackendRequestServerImplConfig.allowCache) {
        val storage: ScalaCache[String] =  CaffeineCache[String]
        Some(new CacheImpl(new StorageImpl(storage)))
    } else None

    val apiProcessor: ActorRef = system.actorOf(ApiRequestProcessor.props(backendRequestServer))

    val userService = new UserRegistryImpl(new UserStorageMockImpl())

    val apiModule = new ApiModule(apiProcessor, userService, cache)

    try {
        Http().bindAndHandle(apiModule.routes, ServerConfig.interface, ServerConfig.port)
        logger.info(s"Server online @ http://${ServerConfig.interface}:${ServerConfig.port}/")
    } catch {
        case NonFatal(_) => system.terminate()
    }

    Await.result(system.whenTerminated, Duration.Inf)
}

