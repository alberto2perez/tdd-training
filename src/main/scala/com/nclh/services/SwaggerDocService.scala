package com.nclh.services

import com.github.swagger.akka.SwaggerHttpService
import com.github.swagger.akka.model.Info
import com.nclh.config.SystemConfig.{HttpServerConfig, ServerConfig}
import com.nclh.routes._

object SwaggerDocService extends SwaggerHttpService {
    override val apiClasses = Set(classOf[ToolsRoutes], classOf[UserRoutes], classOf[WeatherRoutes])
    override val host = s"${HttpServerConfig.hostname}:${ServerConfig.port}"
    override val info = Info(version = "1.0", description = "Say something about this template.", title = "akka test1")
    override val unwantedDefinitions = Seq("Function1", "Function1RequestContextFutureRouteResult")
}
