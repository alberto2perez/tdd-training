package com.nclh.services.user

import akka.Done
import com.nclh.api.models.v1.users.User

import scala.concurrent.Future

trait UserStorage {
    def getAll(): Future[List[User]]
    def create(user: User): Future[Done]
    def delete(name: String): Future[Done]
    def get(name: String): Future[Option[User]]
}
