package com.ncl.services.user

import akka.Done
import com.nclh.api.models.v1.users.User

import scala.concurrent.Future

trait UserRegistry {

    /**
      * Gets the list of all users
      * @return
      */
    def getAll(): Future[List[User]]

    /**
      * Gets a single user from the name
      * @param name
      * @return
      */
    def getUser(name: String): Future[Option[User]]

    /**
      * Create a single user
      * @param user
      * @return
      */
    def createUser(user: User): Future[Done]

    /**
      * Deletes a User
      * @param name
      * @return
      */
    def deleteUser(name: String): Future[Done]

}
