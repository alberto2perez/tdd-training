package com.nclh.services.user

import akka.Done
import com.nclh.api.models.v1.users.User

import scala.concurrent.{ExecutionContext, Future}

// This class simulates a IO abstraction layer like access to a cache, or to a database
class UserStorageMockImpl(implicit ec: ExecutionContext) extends UserStorage {

    /*WARNING! DO NOT use mutable variables on your code, this is a simulation */
    var users = List[User](
        User("John Doe", 22, "US"),
        User("Joanna Doe", 21, "US")
    )

    def getAll(): Future[List[User]] = {
        Future.successful(users)
    }

    def create(user: User): Future[Done] = {
        Future {
            users :+ user
        }.map(_ => Done)
    }

    def delete(name: String): Future[Done] = {
        users = users.filter(u => u.name.equals(name))
        Future(Done)
    }

    def get(name: String): Future[Option[User]] = {
        Future.successful(users.find(u => u.name.equalsIgnoreCase(name)))
    }
}
