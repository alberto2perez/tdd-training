package com.nclh.services.user

import akka.Done
import akka.actor.{Actor, ActorLogging, Props}
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import com.nclh.api.models.v1.users.User
import akka.pattern.pipe
import akka.stream.ActorMaterializer

import scala.concurrent.ExecutionContext

object UserRegistryActor extends SprayJsonSupport {

  final case class UsersList(users: List[User])

  final case object GetUsers

  final case class CreateUser(user: User)

  final case object UserCreated

  final case class GetUser(name: String)

  final case class UserRetrieved(user: Option[User])

  final case class DeleteUser(name: String)

  final case object UserDeleted

  def props(userStorage: UserStorage)(implicit ec: ExecutionContext, actorMaterializer: ActorMaterializer): Props =
    Props(new UserRegistryActor(userStorage))
}

class UserRegistryActor(userStorage: UserStorage)(implicit ec: ExecutionContext) extends Actor with ActorLogging {

  import UserRegistryActor._

  def receive: Receive = {
    case GetUsers =>
      userStorage.getAll().map(u => UsersList(u)).pipeTo(self)(sender())

    case UsersList(users) =>
      sender ! users

    case CreateUser(user) =>
      userStorage.create(user).map(_ => UserCreated).pipeTo(self)(sender)

    case UserCreated =>
      sender() ! Done

    case GetUser(name) =>
      userStorage.get(name).map(u => UserRetrieved(u)).pipeTo(self)(sender())

    case UserRetrieved(u) =>
      sender ! u

  }
}
