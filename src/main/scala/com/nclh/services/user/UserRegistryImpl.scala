package com.nclh.services.user

import akka.Done
import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import com.ncl.services.user.UserRegistry
import com.nclh.api.models.v1.users.User
import com.nclh.services.user.UserRegistryActor.{CreateUser, DeleteUser, GetUser, GetUsers}
import akka.pattern.ask
import akka.util.Timeout

import scala.concurrent.{ExecutionContext, Future}

/*This is an example of an actor based service, uses an actor behind the scenes but
* implements a contract to make it testable and easy to change to another implementation if needed
* */
class UserRegistryImpl(storage: UserStorage)
                      (implicit ec: ExecutionContext, actorMaterializer: ActorMaterializer, actorSystem: ActorSystem, timeout: Timeout)
    extends UserRegistry {

    lazy val actor = actorSystem.actorOf(UserRegistryActor.props(storage))

    /**
      * Gets the list of all users
      *
      * @return
      */
    def getAll(): Future[List[User]] =
        (actor ? GetUsers).mapTo[List[User]]


    /**
      * Gets a single user from the name
      *
      * @param name
      * @return
      */
    def getUser(name: String): Future[Option[User]] =
        (actor ? GetUser(name)).mapTo[Option[User]]

    /**
      * Create a single user
      *
      * @param user
      * @return
      */
    def createUser(user: User): Future[Done] =
        (actor ? CreateUser(user)).mapTo[Done]

    /**
      * Deletes a User
      *
      * @param name
      * @return
      */
    def deleteUser(name: String): Future[Done] =
        (actor ? DeleteUser(name)).mapTo[Done]

}
