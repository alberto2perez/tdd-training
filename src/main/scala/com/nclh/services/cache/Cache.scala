package com.nclh.services.cache

import scala.concurrent.Future
import scala.reflect.ClassTag
import scala.reflect.runtime.universe._

trait Cache {

  /**
    * Gets an object of type T stored by key
    *
    * @param key
    * @return
    */

  def get[T](key: Int)(implicit tag: ClassTag[T]): Future[Option[T]]

  /**
    * Gets a value stored by key, if doesn't exist use resolver to calculate/get the value with a given Future
    * Recommended to use whenever is possible, it optimize all future requests to the same key
    * to run the resolver function only once.
    *
    * @param key
    * @return
    */
  def getOrElseFuture[T](key: Int, resolver: => Future[T], expiresInSeconds: Int)(implicit tag: ClassTag[T]): Future[T]

  /**
    * Gets a value stored by key, if doesn't exist use resolver to calculate/get the value
    * Recommended to use whenever is possible, it optimize all future requests to the same key
    * to run the resolver function only once.
    *
    * @param key
    * @return
    */
  def getOrElse[T](key: Int, resolver: => T, expiresInSeconds: Int)(implicit tag: ClassTag[T]): Future[T]

  /**
    * Saved string data associated with an id
    *
    * @param id
    * @param data
    * @return
    */
  def set[T](id: Int, data: T, expiresInSeconds: Int)(implicit tag: ClassTag[T]): Future[Boolean]

  /**
    * Clears the cache
    *
    * @return
    */
  def clear(): Future[Boolean]

  /**
    * Deletes a single key from the cache
    *
    * @return
    */
  def remove(key: Int): Future[Boolean]

}
