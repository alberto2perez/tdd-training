package com.nclh.services.cache

import scala.concurrent.Future

trait Storage {
  /**
    * Gets a value stored by key
    *
    * @param key
    * @return
    */
  def get(key: Int): Future[Option[String]]

  /**
    * Saved data associated with an id
    *
    * @param id
    * @param data
    * @return
    */
  def set(id: Int, data: String, expiresInSeconds: Int): Future[Boolean]

  /**
    * Removes the cached item with a given id
    *
    * @param id
    * @return
    */
  def remove(id: Int): Future[Boolean]

  /**
    * Cleans up the whole cache
    */
  def deleteAll(): Future[Boolean]

}
