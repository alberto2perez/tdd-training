package com.nclh.services.cache

import java.io.{ByteArrayInputStream, ByteArrayOutputStream, ObjectInputStream, ObjectOutputStream}
import java.util.Base64

import com.nclh.utils.LoggingTry

import scala.concurrent.{ExecutionContext, Future}
import scala.language.postfixOps
import scala.reflect.ClassTag
import scala.util.{Failure, Success}
import scala.reflect.runtime.universe._

class CacheImpl(storage: Storage)(implicit ec: ExecutionContext) extends Cache {

  def get[T](key: Int)(implicit tag: ClassTag[T]): Future[Option[T]] = {
    storage.get(key).map { str =>
      if (str.isDefined)
        Some(if (tag.equals(typeOf[String])) str.get.asInstanceOf[T] else deserialize(str.get).asInstanceOf[T])
      else None
    }
  }

  def getOrElseFuture[T](key: Int, findCacheValue: => Future[T], expiresInSeconds: Int)(implicit tag: ClassTag[T]): Future[T] = {
    get[T](key).flatMap {
      case Some(str) => Future.successful(str)
      case _ => {
        LoggingTry(findCacheValue) match {
          case Success(value) =>
            value.map { v =>
              set[T](key, v, expiresInSeconds)
              v
            }
          case Failure(f) => throw f
        }
      }
    }
  }

  def getOrElse[T](key: Int, findCacheValue: => T, expiresInSeconds: Int)(implicit tag: ClassTag[T]): Future[T] = {
    get[T](key).map {
      case Some(str) => str
      case _ => {
        LoggingTry(findCacheValue) match {
          case Success(v) =>
            set[T](key, v, expiresInSeconds)(tag)
            v
          case Failure(f) => throw f
        }
      }
    }
  }

  def remove(key: Int): Future[Boolean] = storage.remove(key)

  def set[T](key: Int, data: T, expiresInSeconds: Int)(implicit tag: ClassTag[T]): Future[Boolean] = {
    storage.set(key, if (tag.equals(typeOf[String])) data.asInstanceOf[String] else serialize[T](data), expiresInSeconds)
  }

  def clear(): Future[Boolean] = {
    storage.deleteAll()
  }

  def serialize[T](value: T): String = {
    val stream: ByteArrayOutputStream = new ByteArrayOutputStream()
    val oos = new ObjectOutputStream(stream)
    oos.writeObject(value)
    oos.close()
    val bytes = stream.toByteArray
    Base64.getEncoder.encodeToString(bytes)
  }

  def deserialize[T](str: String): T = {
    val bytes: Array[Byte] = Base64.getDecoder.decode(str)
    val ois = new ObjectInputStream(new ByteArrayInputStream(bytes))
    val value = ois.readObject
    ois.close()
    value.asInstanceOf[T]
  }

}

