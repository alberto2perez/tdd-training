package com.nclh.services.cache

import com.nclh.config.SystemConfig
import com.nclh.utils.LoggingTryFuture
import scalacache.modes.scalaFuture._
import scalacache.{Cache => ScalaCache}

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}
import scala.language.postfixOps
import scala.util.Success

class StorageImpl(val scDriver: ScalaCache[String])(implicit ec: ExecutionContext) extends Storage {

  def get(id: Int): Future[Option[String]] = scDriver.get(id)

  def set(id: Int, data: String, expiresInSeconds: Int = SystemConfig.BackendRequestServerImplConfig.cacheTimeout): Future[Boolean] = {
    scDriver.put(id)(data, Some(expiresInSeconds seconds)).map {
      case () => true
      case _  => false
    }
  }

  def remove(id: Int): Future[Boolean] = {
    LoggingTryFuture(scDriver.remove(id)).map {
      case Success(_) => true
      case _ => false
    }
  }

  def deleteAll(): Future[Boolean] =
    LoggingTryFuture(scDriver.removeAll()).map {
      case Success(_) => true
      case _ => false
    }

}
