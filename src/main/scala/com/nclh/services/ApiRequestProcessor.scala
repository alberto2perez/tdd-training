package com.nclh.services

import akka.actor.{Actor, ActorLogging, Props}
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.StatusCodes.InternalServerError
import akka.stream.ActorMaterializer
import akka.pattern.pipe
import com.nclh.config.SystemConfig
import com.nclh.services.ApiRequestProcessor.Protocol._
import com.nclh.services.cache.Cache
import com.nclh.utils.BackendCircuitBreaker

import scala.concurrent.{ExecutionContext, Future}
import scala.util.Failure

class ApiRequestProcessor(backendRequestServer: RequestServer)
                         (implicit materializer: ActorMaterializer, ec: ExecutionContext, cache: Option[Cache]) extends Actor
    with BackendCircuitBreaker with ActorLogging {

    def receive: Receive = processorReceive

    private def makeRequest(request: Request): Future[Response] = {

        circuitBreaker.withCircuitBreaker(
            backendRequestServer.doRequest(request)
        )
    }

    private def getResponse(request: Request): Unit = {
        val returnTo = sender()
        request.cacheId match {
            case Some(id: Int) if cache.isDefined =>
                cache.get.getOrElseFuture[Response](id, makeRequest(request), SystemConfig.BackendRequestServerImplConfig.cacheTimeout).map { data => request -> data }
                    .recover {
                        case err: Error => request -> err
                        case ex: Exception => request -> Error(HttpResponseData(InternalServerError, ex.getMessage))
                    }
                    .pipeTo(self)(returnTo)
            case _ =>
                log.debug("Caching not applicable")
                makeRequest(request).map(response => request -> response)
                    .recover {
                        case err: Error => request -> err
                        case ex: Exception => request -> Error(HttpResponseData(InternalServerError, ex.getMessage))
                    }.pipeTo(self)(returnTo)
        }
    }

    def processorReceive: Receive = {

        case request: Request =>
            log.debug("=== REQUEST ===")
            getResponse(request)

        case (_: Request, result: Result) =>
            sender() ! result

        case (_: Request, err: Error) =>
            sender() ! err

        case Failure(failure) =>
            log.error("Internal failure with ApiRequestProcessor {}", failure.getMessage)
            sender() ! Error(InternalErrorData(s"Internal failure ${failure.getMessage}"))

    }

    def breakerOpen: Unit = log.warning(s"Circuit Breaker open for $cbResetTimeout")

}

object ApiRequestProcessor {

    def props(backendRequestServer: RequestServer)
             (implicit materializer: ActorMaterializer, ec: ExecutionContext, cache: Option[Cache]): Props =
        Props(new ApiRequestProcessor(backendRequestServer))

    object Protocol {

        sealed trait Request {
            val cacheId: Option[Int]
            val data: RequestData
        }

        trait RequestData

        case class SimpleHttpRequest(cacheId: Option[Int], data: HttpRequestData) extends Request

        case class HttpRequestData(url: String,
                                   body: String,
                                   httpMethod: HttpMethod,
                                   contentType: ContentType.WithFixedCharset = ContentTypes.`application/json`
                                  ) extends RequestData

        sealed trait Response {
            val data: ResponseData
        }

        case class Result(data: ResponseData) extends Response

        case class Error(data: ResponseData) extends Throwable with Response

        trait ResponseData {
            val data: Any
        }

        case class HttpResponseData(status: StatusCode, data: String) extends ResponseData

        case class InternalErrorData(data: String) extends ResponseData

    }

}
