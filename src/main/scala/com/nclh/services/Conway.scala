package com.nclh.services

import scala.util.Random

case class Node(
                   id: String,
                   nodes: Map[Int, Node],
                   isLive: Boolean
               ) {


    def liveNodes(): Int = {
        nodes.values.count(_.isLive)
    }

    def isUnderpopulated(): Boolean = {
        isLive && liveNodes() < 2
    }

    def isOverpopulated(): Boolean = {
        isLive && liveNodes() > 3
    }

    def passToNextGen(): Boolean = {
        isLive && (liveNodes().equals(3) || liveNodes().equals(2))
    }

    def willRevive(): Boolean = {
        !isLive && liveNodes().equals(3)
    }
    def willDie(): Boolean = {
        isLive && (liveNodes() < 2 || liveNodes() > 3)
    }


}

object Node {
    def apply(
                 nodes: Map[Int, Node],
                 isLive: Boolean
             ): Node = new Node(Random.nextString(10), nodes, isLive)

    def apply(
                 id: String,
                 nodes: Map[Int, Node],
                 isLive: Boolean
             ): Node = new Node(id, nodes, isLive)

}

class Conway(
                var nodes: List[Node]
            ) {

    def run(): Unit = {

    }
}

