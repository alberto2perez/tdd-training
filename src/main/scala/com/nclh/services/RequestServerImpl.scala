package com.nclh.services

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.StatusCodes.BadGateway
import akka.http.scaladsl.model._
import akka.stream.ActorMaterializer
import akka.util.ByteString
import com.nclh.services.ApiRequestProcessor.Protocol._
import com.typesafe.scalalogging.LazyLogging

import scala.concurrent.{ExecutionContext, Future}

class RequestServerImpl(implicit val system: ActorSystem,
                        implicit val materializer: ActorMaterializer,
                        implicit val ec: ExecutionContext
                       ) extends RequestServer with LazyLogging {

  val httpClientServer = Http(system)

  def doRequest(request: Request): Future[Response] = {
    request.data match {
      case http@HttpRequestData(_, _, _, _) => doHttpRequest(http)
      case _ => throw new RuntimeException("Request type not supported.")
    }
  }

  private def doHttpRequest(request: HttpRequestData): Future[Response] = {
    val entity = HttpEntity(request.contentType, request.body)
    logger.debug(s"The entity is = {}", entity)
    val uri = Uri(request.url)
    val httpRequest = HttpRequest(request.httpMethod, uri, entity = entity)
    val httpResponse = httpClientServer.singleRequest(httpRequest)

    logger.debug("=== RESPONSE ===")
    httpResponse flatMap {
      case HttpResponse(StatusCodes.OK, _, entity: ResponseEntity, _) =>
        entity.dataBytes.runFold(ByteString(""))(_ ++ _).map { body =>
          logger.debug("Got response, body {} ", body.utf8String)
          Result(HttpResponseData(StatusCodes.OK, body.utf8String))
        }

      case resp@HttpResponse(code, _, _, _) =>
        logger.warn("Request failed, response code {} ", code)
        resp.discardEntityBytes()
        throw Error(HttpResponseData(code, "Failure response from backend."))

    } recover {
      case err: Error => throw err
      case _ => throw Error(HttpResponseData(BadGateway, "Http Backend is down."))
    }
  }

}

