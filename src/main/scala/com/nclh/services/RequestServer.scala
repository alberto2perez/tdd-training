package com.nclh.services

import com.nclh.services.ApiRequestProcessor.Protocol.{Request, Response}

import scala.concurrent.Future

trait RequestServer {
    def doRequest(request: Request): Future[Response]
}
