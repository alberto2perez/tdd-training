package com.nclh.config

import com.typesafe.config.ConfigFactory

object SystemConfig {
    val config = ConfigFactory.load()

    object ServerConfig {
        private val serverConfig = config.getConfig("server")
        lazy val systemName: String = serverConfig.getString("system-name")
        lazy val port: Int = serverConfig.getInt("port")
        lazy val interface: String = serverConfig.getString("interface")
        lazy val defaultTimeout: Int = serverConfig.getInt("request-timeout")
    }

    object BackendRequestServerImplConfig{
        private val backendRequestServerImplConfig = config.getConfig("backend-service")
        lazy val endpoint: String = backendRequestServerImplConfig.getString("endpoint")
        lazy val allowCache: Boolean = backendRequestServerImplConfig.getBoolean("allowCache")
        lazy val cacheTimeout: Int = backendRequestServerImplConfig.getInt("cacheTimeout")
    }

    object HttpServerConfig {
        private val httpServerConfig = config.getConfig("http-server")
        lazy val protocol: String = httpServerConfig.getString("protocol")
        lazy val hostname: String = httpServerConfig.getString("hostname")
        lazy val apiPrefix: String = httpServerConfig.getString("api-prefix")
        lazy val apiVersion: String = httpServerConfig.getString("api-version")
        lazy val healthCheck: String = httpServerConfig.getString("health-check")
        lazy val version: String = httpServerConfig.getString("version")
    }

    object BackendCircuitBreakerConfig {
        private val circuitBreakerConfig = config.getConfig("circuit-breaker")
        lazy val maxFailures: Int = circuitBreakerConfig.getInt("maxFailures")
        lazy val callTimeout: Int = circuitBreakerConfig.getInt("callTimeout")
        lazy val resetTimeout: Int = circuitBreakerConfig.getInt("resetTimeout")
    }

    object Docs {
        lazy val enabled: Boolean = config.getBoolean("api-docs.enabled")
    }

    object Tools {
        lazy val tools = config.getConfig("tools")
        lazy val branchFile: String = tools.getString("branch.file")
        lazy val revisionFile: String = tools.getString("revision.file")
        lazy val versionFile: String = tools.getString("version.file")
        lazy val defaultErrorMessage: String = tools.getString("defaultErrorMessage")

        lazy val healthCheckFileLocal: String = config.getString("health-check-path.local")
        lazy val healthCheckFileGlobal: String = config.getString("health-check-path.global")
        lazy val toolsDefaultErrorMessage: String = config.getString("tools.defaultErrorMessage")
    }
}
