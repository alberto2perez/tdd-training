package com.nclh.utils

import com.typesafe.scalalogging.Logger

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.control.NonFatal
import scala.util.{Failure, Success, Try}

object LoggingTry {
    val logger = Logger(this.getClass)
    def apply[T](r: => T): Try[T] =
        try Success(r) catch {
            case NonFatal(e) => {
                logger.error(s"${e.getMessage}: ${e.printStackTrace()}")
                Failure(e)
            }
        }
}

object LoggingTryFuture {
    val logger = Logger(this.getClass)
    def apply[T](r: => Future[T]): Future[Try[T]] = {
        try {
            r.map(Success(_))
        } catch {
            case NonFatal(e) => {
                logger.error(s"${e.getMessage}: ${e.printStackTrace()}")
                Future.successful(Failure(e))
            }
        }
    }
}
