package com.nclh.utils

import scala.io.Source
import scala.util.Try

object Utils {
    def getLocalFile(path: String, defaultValue: String): String = {
        Try {
            val source = Source.fromFile(path)
            val content = source.mkString
            source.close
            content.stripLineEnd
        }.toOption.getOrElse(defaultValue)
    }
}
