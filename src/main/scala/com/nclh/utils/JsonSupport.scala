package com.nclh.utils

import de.heikoseeberger.akkahttpjson4s.Json4sSupport
import org.joda.time._
import org.json4s.JsonAST.{JDouble, JInt, JString}
import org.json4s._

trait JsonSupport extends Json4sSupport {

    implicit val serialization: Serialization = jackson.Serialization
    implicit val formats: Formats = DefaultFormats + new NumberSerializer() +
        new DateTimeSerializer() +
        new LocalDateSerializer() +
        new LocalTimeSerializers()

}

class NumberSerializer extends CustomSerializer[Int](_ => ( {
    case JInt(x) => x.toInt
    case JDouble(x) => x.toInt
    case JString(x) => x.toInt
}, {
    case x: Int => JInt(x)
}
))

class DateTimeSerializer extends CustomSerializer[DateTime](_ => ( {
    case JString(x) => DateUtils.parseDateTimeStr(x)
}, {
    case x: DateTime => JString(DateUtils.getDateTimeInFormat(x))
}
))

class LocalDateSerializer extends CustomSerializer[LocalDate](_ => ( {
    case JString(x) => DateUtils.parseDateStr(x)
}, {
    case x: LocalDate => JString(DateUtils.getLocalDateInFormat(x))
}
))

class LocalTimeSerializers extends CustomSerializer[LocalTime](_ => ( {
    case JString(x) => DateUtils.parseTimeStr(x)
}, {
    case x: LocalTime => JString(DateUtils.getLocalTimeInFormat(x))
}
))
