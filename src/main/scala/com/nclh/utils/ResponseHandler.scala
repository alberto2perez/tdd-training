package com.nclh.utils

import com.nclh.services.ApiRequestProcessor.Protocol
import com.nclh.services.ApiRequestProcessor.Protocol.Response

import scala.concurrent.{ExecutionContext, Future}

case class RequestError(responseError: Protocol.Error) extends Throwable

object ResponseHandler {

    implicit class FutureResponseToModelConverter(response: Future[Response])(implicit val ec: ExecutionContext) {
        def handle[R](block: Future[Response] => Future[R]): Future[R] = {
            block(response map {
                case r: Protocol.Error => throw RequestError(r)
                case r: Response => r
            })
        }
    }

}
