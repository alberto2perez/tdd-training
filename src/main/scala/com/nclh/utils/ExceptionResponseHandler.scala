package com.nclh.utils

import akka.actor.ActorSystem
import akka.event.Logging
import akka.http.scaladsl.model.StatusCodes.{BadGateway, BadRequest, InternalServerError, Unauthorized}
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives.{complete, extractUri}
import akka.http.scaladsl.server.ExceptionHandler
import io.swagger.annotations.ApiModelProperty
import org.json4s.jackson.Serialization.write

object ErrorCodes {
    val BadRequest = "VALIDATION_ERROR"
    val Forbidden = "FORBIDDEN"
    val Unauthorized = "UNAUTHORIZED"
    val BadGateway = "BAD_GATEWAY"
    val InternalError = "ERROR"
}

case class ErrorResponse(
                            @ApiModelProperty(value = "Error Code", example = "ERROR")
                            code: String,
                            @ApiModelProperty(value = "Message", example = "Internal Server Error")
                            message: String
                        )

trait ExceptionResponseHandler extends JsonSupport {

    val errorsLogger = Logging(ActorSystem(), classOf[ExceptionResponseHandler])

    val readableErrorCodes: Map[StatusCode, String] = Map(
        BadRequest -> ErrorCodes.BadRequest,
        Unauthorized -> ErrorCodes.Unauthorized,
        BadGateway -> ErrorCodes.BadGateway,
        InternalServerError -> ErrorCodes.InternalError
    )

    def exceptionHandler = ExceptionHandler {
        case t: Throwable =>
            extractUri { uri: Uri =>
                errorsLogger.error("Unexpected error occurred serving uri [{}]: {}", uri.path, t.getMessage)
                t.printStackTrace()
                val response = ErrorResponse(code = ErrorCodes.InternalError, message = "Internal Server Error")
                complete(
                    HttpResponse(status = InternalServerError, entity = HttpEntity(ContentType(MediaTypes.`application/json`), write(response)))
                )
            }
    }
}
