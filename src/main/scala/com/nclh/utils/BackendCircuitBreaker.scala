package com.nclh.utils

import akka.actor.Actor
import akka.pattern.CircuitBreaker
import com.nclh.config.SystemConfig.BackendCircuitBreakerConfig

import scala.concurrent.duration._
import scala.language.postfixOps

trait BackendCircuitBreaker {
  self: Actor =>

  import context.dispatcher

  val cbMaxFailures: Int = BackendCircuitBreakerConfig.maxFailures
  val cbCallTimeout: FiniteDuration = BackendCircuitBreakerConfig.callTimeout seconds
  val cbResetTimeout: FiniteDuration = BackendCircuitBreakerConfig.resetTimeout minutes

  val circuitBreaker: CircuitBreaker =
    new CircuitBreaker(
      context.system.scheduler,
      maxFailures = cbMaxFailures,
      callTimeout = cbCallTimeout,
      resetTimeout = cbResetTimeout).onOpen(breakerOpen)

  def breakerOpen: Unit


}
