package com.nclh.utils

import org.joda.time.format.DateTimeFormat
import org.joda.time.{DateTime, LocalDate, LocalTime}

object DateUtils {

    val defaultDateTimeFormat = "yyyy-MM-dd HH:mm:ss"
    val defaultLocalDateFormat = "yyyy-MM-dd"
    val defaultLocalTimeFormat = "HH:mm"

    def parseDateStr(date: String, inputFormat: String = defaultLocalDateFormat): LocalDate = {
        DateTime.parse(date, DateTimeFormat.forPattern(inputFormat)).toLocalDate
    }

    def parseDateTimeStr(date: String, inputFormat: String = defaultDateTimeFormat): DateTime = {
        DateTime.parse(date, DateTimeFormat.forPattern(inputFormat))
    }

    def parseTimeStr(date: String, inputFormat: String = defaultLocalTimeFormat): LocalTime = {
        DateTime.parse(date, DateTimeFormat.forPattern(inputFormat)).toLocalTime
    }

    def getDateTimeInFormat(date: DateTime, format: String = defaultDateTimeFormat): String =
        DateTimeFormat.forPattern(format).print(date)

    def getLocalDateInFormat(date: LocalDate, format: String = DateUtils.defaultLocalDateFormat): String =
        DateTimeFormat.forPattern(format).print(date)

    def getLocalTimeInFormat(date: LocalTime, format: String = DateUtils.defaultLocalTimeFormat): String =
        DateTimeFormat.forPattern(format).print(date)
}

