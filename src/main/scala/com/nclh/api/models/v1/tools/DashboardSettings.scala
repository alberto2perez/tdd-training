package com.nclh.api.models.v1.tools

import com.nclh.config.SystemConfig.{HttpServerConfig, BackendRequestServerImplConfig}

case class DashboardSettings(
                                cacheTimeout: Int,
                                version: String,
                                backendServiceUrl: String,
                                backendAllowCache: Boolean
                            )

object DashboardSettings {
    def apply(
                 cacheTimeout: Int = BackendRequestServerImplConfig.cacheTimeout,
                 version: String = HttpServerConfig.apiVersion,
                 backendServiceUrl: String = BackendRequestServerImplConfig.endpoint,
                 backendAllowCache: Boolean  = BackendRequestServerImplConfig.allowCache
             ): DashboardSettings =

        new DashboardSettings(cacheTimeout,
            version,
            backendServiceUrl,
            backendAllowCache)
}
