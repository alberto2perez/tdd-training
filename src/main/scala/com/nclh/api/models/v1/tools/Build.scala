package com.nclh.utils

case class Build(branch: String, revision: String, version: String)
