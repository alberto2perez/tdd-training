package com.nclh.api.models.v1.weather

case class WeatherStatus(
                            id: String,
                            weather_state_name: String,
                            weather_state_abbr: String,
                            wind_direction_compass: String,
                            min_temp: String,
                            max_temp: String,
                            the_temp: String,
                            wind_speed: String,
                            wind_direction: String,
                            air_pressure: String,
                            visibility: String,
                            predictability: String
                        )
