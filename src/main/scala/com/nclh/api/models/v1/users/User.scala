package com.nclh.api.models.v1.users

case class User(name: String, age: Int, countryOfResidence: String)
