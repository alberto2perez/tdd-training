package com.nclh.api.controllers

import java.text.DecimalFormat

import akka.actor.ActorRef
import akka.http.scaladsl.model.HttpMethods.GET
import akka.util.Timeout
import com.nclh.api.interfaces.BaseController
import com.nclh.api.models.v1.weather.WeatherStatus
import com.nclh.config.SystemConfig.BackendRequestServerImplConfig
import com.nclh.services.ApiRequestProcessor.Protocol.{HttpRequestData, HttpResponseData, SimpleHttpRequest}
import com.nclh.utils.JsonSupport
import org.joda.time.LocalDate
import org.json4s.jackson.Serialization

import scala.concurrent.{ExecutionContext, Future}

class WeatherController(apiProcessor: ActorRef)(implicit timeout: Timeout, implicit override val ec: ExecutionContext) extends BaseController(timeout) with JsonSupport {
    /**
      * Backend Url to consume from.
      */
    protected val endpoint = BackendRequestServerImplConfig.endpoint

    def getWeather(date: LocalDate): Future[Seq[WeatherStatus]] = {
        val df = new DecimalFormat("00")
        val url = s"$endpoint/api/location/2450022/${date.getYear}/${df.format(date.getMonthOfYear)}/${df.format(date.getDayOfMonth)}/"

        getResponse[HttpResponseData, String](
            apiProcessor,
            SimpleHttpRequest(cacheId = Some(url.hashCode), HttpRequestData(url, "", GET))
        ).map { data =>
            Serialization.read[List[WeatherStatus]](data)
        }
    }
}
