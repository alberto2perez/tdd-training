package com.nclh.api.interfaces

import akka.actor.ActorRef
import akka.http.scaladsl.model.StatusCode
import akka.http.scaladsl.model.StatusCodes.BadRequest
import akka.pattern.ask
import akka.util.Timeout
import com.nclh.services.ApiRequestProcessor.Protocol._
import com.nclh.utils.RequestError
import com.nclh.utils.ResponseHandler._

import scala.concurrent.{ExecutionContext, Future}
import scala.reflect.ClassTag

class BaseController(val t: Timeout)(implicit val ec: ExecutionContext) {

    /**
      * Sends a Request to the processor and receives a ResponseData
      * Applies the given list of filters to the response and returns it
      *
      * @param processor
      * @param req
      * @param rTag
      * @tparam R
      * @tparam D
      * @return
      */
    def getResponse[R <: ResponseData, D](processor: ActorRef,
                                          req: Request
                                         )(implicit rTag: ClassTag[R]): Future[D] = {

        implicit val timeout: Timeout = t
        (processor ? req).mapTo[Response] handle {
            _.map(_.data).mapTo[R](rTag) map { res: R =>
                res.data.asInstanceOf[D]
            }
        }
    }

    /**
      * Sends a Request to the processor and receives a ResponseData
      * Applies the given list of filters to the response
      * Uses the builder to convert the data in the Response
      * to the model of type T and returns it
      *
      * @param processor
      * @param req
      * @param builder
      * @param rTag
      * @tparam R
      * @tparam D
      * @tparam T
      * @return
      */
    def getResponse[R <: ResponseData, D, T](processor: ActorRef,
                                             req: Request,
                                             builder: D => T
                                            )(implicit rTag: ClassTag[R]): Future[T] = {

        implicit val timeout: Timeout = t
        (processor ? req).mapTo[Response] handle {
            _.map(_.data).mapTo[R](rTag) map { res: R =>
                builder(res.data.asInstanceOf[D])
            }
        }
    }

    /**
      * Gracefully extract the value of an option
      * Throws an exception if option is empty
      *
      * @param o
      * @tparam T
      * @return
      */
    def getOrException[T](o: Option[T], status: StatusCode = BadRequest, message: String = "Unknown Error"): T = {
        o match {
            case Some(x) => x
            case _ => throw RequestError(Error(data = HttpResponseData(status, message)))
        }
    }
}
