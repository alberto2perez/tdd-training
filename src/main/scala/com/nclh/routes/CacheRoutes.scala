package com.nclh.routes

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.nclh.utils._
import com.nclh.modules.ApiProcessorSupport

trait CacheRoutes extends ApiProcessorSupport with JsonSupport {

  def cacheRoutes: Route = clearCache ~ deleteSingleCache

  def clearCache: Route = {
    pathPrefix("cache") {
      path("clear") {
        get {
          cacheOpt match {
            case None => complete("Cache is disabled!")
            case _    =>
              complete(cacheOpt.get.clear().map {
                case true   => "Cache has been cleaned!"
                case _      => "Cache was NOT cleared! - check the logs"
              })
          }
        }
      }
    }
  }

  def deleteSingleCache: Route = {
    pathPrefix("cache") {
      path(Segment / "delete") { cacheId =>
        cacheOpt match {
          case None => complete("Cache is disabled!")
          case Some(cache) =>
            complete(
              cache.remove(cacheId.toInt).map {
                case true => s"Cache item with key $cacheId was deleted!"
                case false =>   "Cache item was not deleted, check the logs"
              }
            )
        }
      }
    }
  }
}
