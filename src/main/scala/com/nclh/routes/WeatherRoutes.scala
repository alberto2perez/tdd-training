package com.nclh.routes

import akka.http.scaladsl.server.{Directives, Route}
import com.nclh.api.controllers.WeatherController
import com.nclh.api.models.v1.weather.WeatherStatus
import com.nclh.modules.ApiProcessorSupport
import com.nclh.utils.{DateUtils, JsonSupport}
import com.typesafe.scalalogging.LazyLogging
import io.swagger.annotations._

import scala.util.Try

@Api(value = "/weather", produces = "application/json")
trait WeatherRoutes extends Directives with ApiProcessorSupport with JsonSupport with LazyLogging {

    val weatherController = new WeatherController(apiProcessor)

    lazy val weatherRoutes: Route = {
        pathPrefix("weather") {
            getWeather
        }
    }

    @ApiOperation(value = "Get weather", nickname = "getWeather", httpMethod = "GET", response = classOf[WeatherStatus], responseContainer = "List")
    @ApiResponses(Array(new ApiResponse(code = 500, message = "Internal server error")))
    def getWeather = {
        ignoreTrailingSlash {
            get {
                parameter("date") { dateParam =>
                    validate(Try(DateUtils.parseDateStr(dateParam)).toOption.isDefined, "Invalid date parameter, please use something like 2018-11-05") {
                        val date = DateUtils.parseDateStr(dateParam)
                        complete(weatherController.getWeather(date))
                    }
                }
            }
        }
    }
}
