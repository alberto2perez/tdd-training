package com.nclh.routes

import java.nio.file.{Files, Paths}

import akka.http.scaladsl.model.StatusCodes.OK
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.nclh.api.models.v1.tools.DashboardSettings
import com.nclh.config.SystemConfig
import com.nclh.utils.{Build, Utils}
import io.swagger.annotations._
import javax.ws.rs.Path

@Api(value = "/tools", produces = "application/json", tags = Array("Tools"))
@Path("/tools")
trait ToolsRoutes
    extends CacheRoutes
{

    def toolsRoute: Route =
        pathPrefix("tools") {
            dashBoardRoute ~ dashBoardSettingsRoute ~ cacheRoutes ~ getAvailableTools  ~ healthCheck
        }

    @ApiOperation(value = "Get available tools options", nickname = "tools", httpMethod = "GET", produces = "text/html(UTF-8)")
    @ApiResponses(Array(new ApiResponse(code = 500, message = "Internal server error")))
    @Path("/")
    def getAvailableTools: Route = {
        pathEndOrSingleSlash {
            get {
                complete(HttpResponse(
                    OK,
                    entity = HttpEntity(
                        ContentType(
                            MediaTypes.`text/html`,
                            HttpCharsets.`UTF-8`
                        ),
                        Utils.getLocalFile("src/main/resources/tools.html", "N/A")
                    )
                ))
            }
        }
    }

    @ApiOperation(value = "Gets the deployed version", nickname = "dashBoard", httpMethod = "GET", response = classOf[Build])
    @ApiResponses(Array(new ApiResponse(code = 500, message = "Internal server error")))
    @Path("/api/dashBoard")
    def dashBoardRoute: Route = {
        ignoreTrailingSlash {
            path( "api" / "dashBoard") {
                get {
                    complete(OK, Build(
                        Utils.getLocalFile(SystemConfig.Tools.branchFile, "N/A"),
                        Utils.getLocalFile(SystemConfig.Tools.revisionFile, "N/A"),
                        Utils.getLocalFile(SystemConfig.Tools.versionFile, "N/A"))
                    )
                }
            }
        }
    }

    @ApiOperation(value = "Get dashboard settings", nickname = "dashBoardSettings", httpMethod = "GET", response = classOf[DashboardSettings])
    @ApiResponses(Array(new ApiResponse(code = 500, message = "Internal server error")))
    @Path("/api/dashBoardSettings")
    def dashBoardSettingsRoute: Route = {
        ignoreTrailingSlash {
            path( "api" / "dashBoardSettings") {
                get {
                    complete(OK, DashboardSettings())
                }
            }
        }
    }

    @ApiOperation(value = "Get Healthcheck response", nickname = "healthCheck", httpMethod = "GET", response = classOf[String])
    @ApiResponses(Array(new ApiResponse(code = 500, message = "Internal server error")))
    @Path("/healthCheck")
    def healthCheck: Route = {
        path("healthCheck") {
            get {
                complete(OK, healthChecker("N/A"))
            }
        }
    }

    private def healthChecker(defaultValue: String = SystemConfig.Tools.defaultErrorMessage): String = {
        val isExistsLocal = Files.exists(Paths.get(SystemConfig.Tools.healthCheckFileLocal))
        val isExistsGlobal = Files.exists(Paths.get(SystemConfig.Tools.healthCheckFileGlobal))

        if (isExistsLocal && isExistsGlobal) {
            val currentFlagLocal = Utils.getLocalFile(SystemConfig.Tools.healthCheckFileLocal, "N/A")
            val currentFlagGlobal = Utils.getLocalFile(SystemConfig.Tools.healthCheckFileGlobal, "N/A")

            if (currentFlagLocal.equals(currentFlagGlobal)) currentFlagGlobal else SystemConfig.Tools.defaultErrorMessage
        } else {
            defaultValue
        }
    }
}
