package com.nclh.routes

import akka.http.scaladsl.server.{Directives, Route}
import akka.util.Timeout
import com.ncl.services.user.UserRegistry
import com.nclh.api.models.v1.users.User
import com.nclh.modules.ApiProcessorSupport
import com.nclh.services.user.UserRegistryActor.UsersList
import com.nclh.utils.{ActionPerformed, JsonSupport}
import com.typesafe.scalalogging.LazyLogging
import io.swagger.annotations._
import javax.ws.rs.Path

import scala.concurrent.duration._

@Api(value = "/users", produces = "application/json", tags = Array("Users"))
@Path("/users")
trait UserRoutes extends ApiProcessorSupport with Directives with JsonSupport with LazyLogging {

    val usersService: UserRegistry
    implicit val timeout = Timeout(2.seconds)

    lazy val route: Route = {
        pathPrefix("users") {
            getUsers ~ createUser ~ deleteUser
        }
    }

    @ApiOperation(value = "Get users", nickname = "getUsers", httpMethod = "GET", response = classOf[UsersList], responseContainer = "List")
    @ApiResponses(Array(new ApiResponse(code = 500, message = "Internal server error")))
    def getUsers = {
        pathEnd {
            get {
                complete(usersService.getAll())
            }
        }
    }

    @ApiOperation(notes = "Hi, I'm a note", value = "Add user", nickname = "addUser", httpMethod = "POST", response = classOf[ActionPerformed])
    @ApiImplicitParams(Array(new ApiImplicitParam(name = "body", value = "\"users\" to add", required = true, dataTypeClass = classOf[User], paramType = "body")))
    @ApiResponses(Array(new ApiResponse(code = 500, message = "Internal server error")))
    def createUser =
        extractRequest { request =>
            post {
                entity(as[User]) { user =>
                    complete(usersService.createUser(user).map { _ =>
                        logger.info("User has been created {}", user.name)
                        // example of custom json response
                        ActionPerformed("User has been succesfully created")
                    })
                }
            }
        }

    @Path("/{id}")
    @ApiOperation(value = "Delete user", nickname = "deleteUser", httpMethod = "DELETE", response = classOf[ActionPerformed])
    @ApiImplicitParams(Array(new ApiImplicitParam(name = "id", value = "Id of person to delete", required = true, dataType = "string", paramType = "path")))
    @ApiResponses(Array(new ApiResponse(code = 500, message = "Internal server error")))
    def deleteUser: Route =
        path(Segment) { name =>
            delete {
                complete(usersService.deleteUser(name).map { _ =>
                    logger.info("User has been created {}", name)
                    // example of custom json response
                    ActionPerformed("User has been successfully deleted")
                })
            }
        }
}
