package com.nclh.routes

import akka.http.scaladsl.model.{HttpResponse, StatusCodes}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.nclh.config.SystemConfig
import com.nclh.modules.ApiProcessorSupport
import com.nclh.services.SwaggerDocService
import com.nclh.utils.JsonSupport

trait DocRoutes extends ApiProcessorSupport with JsonSupport {

    def docRoutes: Route = {
        if (SystemConfig.Docs.enabled) {
            swaggerRoutes
        } else {
            docsDisabledBySettings
        }
    }

    /**
      * Entry point for all swagger routes.
      *
      * @return
      */
    def swaggerRoutes: Route = {
        localDocs ~ SwaggerDocService.routes
    }

    private def localDocs: Route = {
        path("docs") {
            getFromResource("swagger/index.html")
        } ~
            getFromResourceDirectory("swagger")
    }

    /**
      * Routes for when the docs are disabled.
      *
      * @return
      */
    def docsDisabledBySettings: Route = {
        (pathPrefix("docs") | pathPrefix("api-docs")) {
            complete {
                HttpResponse(
                    status = StatusCodes.Forbidden,
                    entity = "Docs disabled by server settings."
                )
            }
        }
    }
}
