package com.nclh

import com.nclh.services.{Conway, Node}
import org.scalatest._

import scala.language.postfixOps

class ConwaySpec extends WordSpec with MustMatchers {
    "underpopulation" should {
        "a node be with unnderpopulation" in {
            val node = Node(
                Map(
                    0 -> Node(Map(), true),
                    1 -> Node(Map(), false),
                    2 -> Node(Map(), false)
                ),
                true
            )

            assert(node.isUnderpopulated() equals true)
        }
        "a node be with Not underpopulation" in {
            val node = Node(
                Map(
                    0 -> Node(Map(), true),
                    1 -> Node(Map(), true),
                    2 -> Node(Map(), false)
                ),
                true
            )

            assert(node.isUnderpopulated() equals false)
        }
    }
    "live cell dies" should {
        "a node can die" in {
            val node = Node(
                Map(
                    0 -> Node(Map(), true)
                ),
                true
            )

            assert(node.willDie() equals true)
        }
        "a node can not die" in {
            val node = Node(
                Map(
                    0 -> Node(Map(), true),
                    1 -> Node(Map(), true)
                ),
                true
            )

            assert(node.willDie() equals false)
        }
    }
    "overpopulation" should {
        "a node be with overpopulation" in {
            val node = Node(
                Map(
                    0 -> Node(Map(), true),
                    1 -> Node(Map(), true),
                    2 -> Node(Map(), true),
                    3 -> Node(Map(), true)
                ),
                true
            )

            assert(node.isOverpopulated() equals true)
        }
    }
    "pass" should {
        "a node that pass" in {
            val node = Node(
                Map(
                    0 -> Node(Map(), true),
                    1 -> Node(Map(), true),
                    2 -> Node(Map(), false),
                    3 -> Node(Map(), false)
                ),
                true
            )

            assert(node.passToNextGen() equals true)
        }
    }
    "can relive" should {
        "a node can be revived" in {
            val node = Node(
                Map(
                    0 -> Node(Map(), true),
                    1 -> Node(Map(), true),
                    2 -> Node(Map(), true),
                    3 -> Node(Map(), false)
                ),
                false
            )

            assert(node.willRevive() equals true)
        }
    }
    "can relive" should {
        "a node can not be revived" in {
            val node = Node(
                Map(
                    0 -> Node(Map(), true),
                    1 -> Node(Map(), true),
                    2 -> Node(Map(), true),
                    3 -> Node(Map(), false)
                ),
                true
            )

            assert(node.willRevive() equals false)
        }
    }
}
