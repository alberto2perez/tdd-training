import sbt._

object Version {
    val scalaLogging                = "3.7.2"
    val scalaTest                   = "3.0.4"
    val mockito                     = "2.10.0"
    val logstashLogbackEncoder      = "4.11"
    val akkaHttpVersion             = "10.1.1"
    val akkaVersion                 = "2.5.11"
    val akkaSlf4j                   = "2.5.9"
    val logback                     = "1.2.3"
    val akkaCors                    = "0.2.2"
    val jaxBApi                     = "2.3.0"
    val akkaJson                    = "1.19.0"
    val json4s                      = "3.5.3"
    val json4sExt                   = "3.5.3"
    val gatling                     = "2.3.0"
    val joda                        = "2.9.9"
    val config                      = "1.3.1"
    val jacksonDataFormat           = "2.9.6"
    val jacksonVersion              = "2.9.7"
    val swagger                 = "1.0.0"
    val swaggerJaxRS            = "1.5.21"
    val scalaCache              = "0.24.2"
}

object Dependencies {

    val resolvers = Seq(
        Resolver.sonatypeRepo("snapshots"),
        "DynamoDB Local Release Repository" at "https://s3-us-west-2.amazonaws.com/dynamodb-local/release"
    )

    val plugins = Seq(
    )

    private lazy val joda                       = "joda-time"                        % "joda-time"                       % Version.joda
    private lazy val json4s                     = "org.json4s"                       %% "json4s-jackson"                 % Version.json4s
    private lazy val json4sExt                  = "org.json4s"                       %% "json4s-ext"                     % Version.json4sExt
    private lazy val akkaJson                   = "de.heikoseeberger"                %% "akka-http-json4s"               % Version.akkaJson
    private lazy val akkaCors                   = "ch.megard"                        %% "akka-http-cors"                 % Version.akkaCors
    private lazy val akkaSlf4j                  = "com.typesafe.akka"                %% "akka-slf4j"                     % Version.akkaSlf4j
    private lazy val logbackCore                = "ch.qos.logback"                   % "logback-core"                    % Version.logback
    private lazy val logbackClassic             = "ch.qos.logback"                   % "logback-classic"                 % Version.logback
    private lazy val logbackAccess              = "ch.qos.logback"                   % "logback-access"                  % Version.logback
    private lazy val logging                    = "com.typesafe.scala-logging"       %% "scala-logging"                  % Version.scalaLogging
    private lazy val logstashLogbackEncoder     = "net.logstash.logback"             % "logstash-logback-encoder"        % Version.logstashLogbackEncoder
    private lazy val akkaHttpSprayJson          = "com.typesafe.akka"                %% "akka-http-spray-json"           % Version.akkaHttpVersion
    private lazy val akkaStream                 = "com.typesafe.akka"                %% "akka-stream"                    % Version.akkaVersion
    private lazy val akkaHttpTestKit            = "com.typesafe.akka"                %% "akka-http-testkit"              % Version.akkaHttpVersion
    private lazy val akkaTestKit                = "com.typesafe.akka"                %% "akka-testkit"                   % Version.akkaVersion
    private lazy val akkaStreamTestKit          = "com.typesafe.akka"                %% "akka-stream-testkit"            % Version.akkaVersion
    private lazy val scalaTest                  = "org.scalatest"                    %% "scalatest"                      % Version.scalaTest
    private lazy val jacksonDataFormat          = "com.fasterxml.jackson.dataformat" %  "jackson-dataformat-cbor"        % Version.jacksonDataFormat
    private lazy val jacksonDataBind            = "com.fasterxml.jackson.core"       % "jackson-databind"                % Version.jacksonVersion
    private lazy val jacksonModule              = "com.fasterxml.jackson.module"     %% "jackson-module-scala"           % Version.jacksonVersion
    private lazy val swaggerJaxRS           = "io.swagger"                      % "swagger-jaxrs"               % Version.swaggerJaxRS
    private lazy val swagger                = "com.github.swagger-akka-http"    %% "swagger-akka-http"          % Version.swagger
    private lazy val scalaCache             = "com.github.cb372"                %% "scalacache-caffeine"        % Version.scalaCache
    private lazy val mockito                = "org.mockito"                     %  "mockito-core"               % Version.mockito

    val dependencies = Seq(
        akkaHttpTestKit     % Test,
        akkaTestKit         % Test,
        akkaStreamTestKit   % Test,
        scalaTest           % Test,
        swagger,
        swaggerJaxRS,
        logging,
        logbackCore,
        logbackClassic,
        logstashLogbackEncoder,
        akkaHttpSprayJson,
        akkaStream,
        akkaSlf4j,
        akkaCors,
        akkaJson,
        json4s,
        json4sExt,
        joda,
        scalaCache,
        mockito
    )
}
