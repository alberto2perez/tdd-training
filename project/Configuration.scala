import com.typesafe.config.{Config, ConfigFactory}
import com.typesafe.sbt.packager.archetypes.systemloader.ServerLoader.ServerLoader
import sbt.{File, _}

import scala.util.Try
import scala.collection.JavaConversions._

object Configuration {

    lazy val appConf = ConfigFactory.parseFile(new File("conf/application.conf")).resolve()
    val defaultScalacOptions = Seq("-deprecation", "-feature", "-unchecked")

    lazy val useSlick = Try(appConf.getAnyRef("slick")).toOption.isDefined
    lazy val useCache = Try(appConf.getAnyRef("play.cache.active")).toOption.isDefined

    object Build {
        lazy val conf = ConfigFactory.parseFile(new File("conf/build.conf")).resolve()
        lazy val maintainer: String               = conf.getString("build.maintainer")
        lazy val packageDescription: String       = conf.getString("build.packageDescription")
        lazy val packageSummary: String           = conf.getString("build.packageSummary")
        lazy val daemonUser: String               = conf.getString("build.daemonUser")
        lazy val daemonGroup: String              = conf.getString("build.daemonGroup")
        lazy val coverageFailOnMinimum: Boolean   = Try(conf.getBoolean("build.coverage.failOnMinimum")).toOption.getOrElse(false)
        lazy val coverageExcludedPackages: String = Try(conf.getStringList("build.coverage.excludedPackages").toList).toOption.getOrElse(Nil).mkString(" ")
    }
    object Run {
        lazy val conf: Config                        = ConfigFactory.parseFile(new File("conf/run.conf")).resolve()
        lazy val javaOptions: Seq[String]            = Try(conf.getStringList("run.javaOptions").toList).toOption.getOrElse(Nil)
    }
    object Test {
        lazy val conf: Config                        = ConfigFactory.parseFile(new File("conf/test.conf")).resolve()
        lazy val publishArtifact: Boolean            = Try(conf.getBoolean("test.publishArtifact")).toOption.getOrElse(false)
        lazy val scalacOptions: Seq[String]          = defaultScalacOptions ++ Try(conf.getStringList("test.scalacOptions").toList).toOption.getOrElse(Nil)
        lazy val javaOptions: Seq[String]            = Try(conf.getStringList("test.javaOptions").toList).toOption.getOrElse(Nil)
    }
    object Docker {
        lazy val exposedPorts                       = Seq(8080, 9443)
        lazy val repository                         = Some("nexus3.nclmiami.ncl.com:8083")
    }
    object Publish {
        val nexus = "http://nexus.nclmiami.ncl.com/"
        lazy val conf = ConfigFactory.parseFile(new File("conf/publish.conf")).resolve()
        lazy val version                             = Def.setting((sbt.Keys.version in ThisBuild).value)
        lazy val rpmVendor: String                   = Try(conf.getString("publish.rpm.vendor")).toOption.getOrElse("Norwegian Cruise Line Holdings LTD.")
        lazy val rpmLicense: Option[String]          = Try(conf.getString("publish.rpm.license")).toOption
        lazy val rpmUrl: Option[String]              = Try(conf.getString("publish.rpm.url")).toOption
        lazy val rpmGroup: Option[String]            = Try(conf.getString("publish.rpm.group")).toOption
        lazy val packageArchitecture: String         = Try(conf.getString("publish.rpm.arch")).toOption.getOrElse("x86_64")
        lazy val serverLoading: Option[ServerLoader] = Some(com.typesafe.sbt.packager.archetypes.systemloader.ServerLoader.SystemV)
        def publishTo(isSnapshot:Boolean)            = if (isSnapshot) Some("snapshots" at nexus + "content/repositories/snapshots")
                                                       else Some("releases"  at nexus + "content/repositories/releases")
        lazy val credentials                         = Credentials(Path.userHome / ".sbt" / ".ncl-nexus")
        lazy val releaseTagComment: Def.Initialize[String] = Def.setting(s"Releasing ${version.value} [ci skip]")
        lazy val releaseVersionBump = sbtrelease.Version.Bump.Bugfix
        lazy val releaseCommitMessage: Def.Initialize[String] = Def.setting(s"Setting version to ${version.value} [ci skip]")
        lazy val scalacOptions: Seq[String] = defaultScalacOptions ++ Try(conf.getStringList("scalacOptions").toList).toOption.getOrElse(Nil)
        lazy val javaOptions: Seq[String] = Try(conf.getStringList("javaOptions").toList).toOption.getOrElse(Nil)
    }
}