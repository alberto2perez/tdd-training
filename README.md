# TDD Training



### Running

You need to download and install sbt for this application to run.

Once you have sbt installed, the app will start by default on port 8080, just run:

```
sbt run
```

To run in a different port (ex: 9191) use: 

```
sbt 'run 9191'
```

If you need to specify a configuration file other than application.conf, you can pass it as a parameter to sbt:

```
sbt -Dconfig.file=conf/local.conf run
``` 

### Tests

To execute all tests run
```
sbt test
```

To execute a single test run
```
sbt "testOnly com.ncl.routes.AppControllerSpec"
```

To run tests with coverage
```
sbt clean coverage test coverageReport
```

To explore the resulting coverage report run
```
google-chrome src/TDD-Training/target/scala-2.12/scoverage-report/index.html
```
or open the file ```src/TDD-Training/target/scala-2.12/scoverage-report/index.html``` with any other existing browser

### Performance Tests

The best way to see what Akka-Http can do is to run a load test.  We've included Gatling in this test project for integrated load testing.

Start Akka-Http in production mode:

```
sbt stage
cd target/universal/stage
bin/TDD-Training
```

Then you'll start the Gatling load test up (it's already integrated into the project):

```
sbt gatling:test
```

Or to start a single gatling test suite
```
sbt "gatling:testOnly com.ncl.gatling.simulation.StressSimulationSpec"
```

For best results, start the gatling load test up on another machine so you do not have contending resources.  You can edit the [Gatling simulation](http://gatling.io/docs/2.2.2/general/simulation_structure.html#simulation-structure), and change the numbers as appropriate.

Once the test completes, you'll see an HTML file containing the load test chart, in a path similar to:

```
 target/gatling/gatlingspec-1525383368592/index.html
```

That will contain your load test results.
