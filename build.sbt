

name := "tdd-training"
organization in ThisBuild := "com.nclh"
scalaVersion in ThisBuild := "2.12.6"

lazy val akkaHttpVersion = "10.1.4"
lazy val akkaVersion = "2.5.16"
lazy val compilerOptions = Seq(
    "-unchecked",
    "-feature",
    "-language:existentials",
    "-language:higherKinds",
    "-language:implicitConversions",
    "-language:postfixOps",
    "-deprecation",
    "-encoding",
    "utf8"
)


lazy val settings = Seq(
    scalacOptions ++= compilerOptions,
    resolvers ++= Seq(
        Resolver.sonatypeRepo("releases"),
        Resolver.sonatypeRepo("snapshots")
    )
)
/** PROJECTS SETUP **/
lazy val global = project
    .in(file("."))
    .settings(
        name := "tdd-training",
        settings,
        libraryDependencies ++= Dependencies.dependencies,
        resolvers ++= Dependencies.resolvers
    )

